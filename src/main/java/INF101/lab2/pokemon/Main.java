package INF101.lab2.pokemon;
import java.util.Random;

public class Main {

    public static Pokemon pokemon1;
    public static Pokemon pokemon2;

    public static void main(String[] args) {
        ///// Oppgave 6
        // Have two pokemon fight until one is defeated
        int healthPoints1 = 100;
        int strength1 = 10;
        int healthPoints2 = 100;
        int strength2 = 10;
        pokemon1 = new Pokemon("Pikachu", healthPoints1, strength1 );
        pokemon2 = new Pokemon("Oddish" , healthPoints1, strength1 );

        System.out.println(pokemon1.toString());
        System.out.println(pokemon2.toString());
        System.out.println("");

        while (true) {
            pokemon1.attack(pokemon2);
            if ( !pokemon2.isAlive() ) {
                break;
            } else {
                pokemon2.attack(pokemon1);
                if ( !pokemon1.isAlive() ) {
                    break;
                }
            }

        }
    }
}

